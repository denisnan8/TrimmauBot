import express from 'express';

export default class WebServer
{
    constructor(_main)
    {
        const PORT = process.env.PORT || 3000;
        const app  = express();

        app.get('/', function(_req, res) {
            res.send(
                '<img src="https://konachan.com/sample/419c4c3d0eaa88653a4d640b4f47457c/Konachan.com%20-%20311265%20sample.jpg">');
        });
        this.server
            = app.listen(PORT, () => console.log(`Listening on ${PORT}`));
    }
}