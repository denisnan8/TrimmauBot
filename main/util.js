import {readdirSync} from 'fs';
import fetch from 'node-fetch';
import {URL} from 'url';

import discordjs_pkg from 'discord.js-light';
const { MessageAttachment } = discordjs_pkg;

import {db, RAYSHIFT_URL} from './const.js';

const UNAVAILABLE_TEXT = 'Unavailable';
const LOADING_TEXT     = 'Loading...';
const SUPPORT_IMG_NAME = 'support.png';

export default class Util
{
    constructor(main) { this.main = main; }

    weighted_table_generator(item_probabilities_map)
    {
        let table = [];
        for (const item in item_probabilities_map)
        {
            for (let prob = 0; prob < item_probabilities_map[item]; prob++)
            { table.push(item); }
        }
        return function() {
            return table[Math.floor(Math.random() * table.length)];
        };
    }

    chunk_array(array, chunk_size)
    {
        return Array.from({ length : Math.ceil(array.length / chunk_size) },
                          (_, index) => array.slice(index * chunk_size,
                                                    (index + 1) * chunk_size));
    }

    capitalize(string)
    {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    is_valid_url(url)
    {
        try
        {
            new URL(url);
            return true;
        }
        catch (err)
        {
            return false;
        }
    }

    format_friend_code(id)
    {
        const parts = id.match(/\d{3}/g);
        return parts && parts.join(',') || id;
    }

    // incl min, incl max
    rand(min, max)
    {
        max = max || 1;
        min = min || 0;
        if (max < min) return false;
        return Math.round((Math.random() * (max - min)) + min);
    }

    strip_color_code_from_fgo_ign(name)
    {
        return name.replace(/\[[0-9a-f]{6}\]/gi, '');
    }

    ARand(array)
    {
        if (array.length == 1) return array[0];
        return array[this.rand(0, array.length - 1)];
    }

    fgoProfile(user, data)
    {
        let embed = {
            title : 'F/GO Profile for ' + user.username,
            fields : [
                { name : 'IGN', value : data.name || 'Not Provided' },
                { name : 'Friend ID', value : data.id || 'Not Provided' }
            ],
            description : '\u200b'
        };
        if (data.support && this.is_valid_url(data.support))
            embed.image = { url : data.support };
        return embed;
    }

    async fgoProfileRayshift(user, data, is_loading = false)
    {
        let embed = {
            author : {
                name : 'Rayshift.io',
                icon_url : `${RAYSHIFT_URL}favicon-32x32.png`,
                url : is_loading ? null
                                 : `${RAYSHIFT_URL}${data.region}/${data.code}`,
            },
            title : `${this.capitalize(data.deck_type)} profile for ${
                user.username}`,
            fields : [
                {
                    name : 'IGN',
                    value : is_loading
                                ? LOADING_TEXT
                                : this.strip_color_code_from_fgo_ign(data.name)
                                      || UNAVAILABLE_TEXT,
                },
                {
                    name : 'Friend ID',
                    value : is_loading ? LOADING_TEXT
                                       : this.format_friend_code(data.code)
                                             || UNAVAILABLE_TEXT,
                    inline : true
                },
                {
                    name : 'Region',
                    value : data.region.toUpperCase() || UNAVAILABLE_TEXT,
                    inline : true
                },
            ],
            timestamp : is_loading ? null : new Date(data.lastUpdate * 1000),
            footer : { text : is_loading ? null : 'Last update' }
        };

        let files = [];
        if (data.support && this.is_valid_url(data.support)) {
            const image_stream =
                await fetch(data.support).then(res => res.buffer()).catch(console.log);
            files.push(new MessageAttachment(image_stream, SUPPORT_IMG_NAME));
            embed.image = { url : `attachment://${SUPPORT_IMG_NAME}` };
        }

        return { files: files, embed: embed };
    }

    fgoItem(args)
    {
        return new Promise((resolve, _reject) => {
            let item = false;
            fetch(`${db}item.json`).then(res => res.json()).then(r => {
                if (!(item = r[args.toUpperCase()]))
                {
                    for (let index in r)
                    {
                        let i = r[index];
                        if (!item && i.name.toLowerCase().includes(args))
                        {
                            item    = i;
                            item.id = index;
                        }
                    }
                }
                else
                {
                    item.id = args.toUpperCase();
                }
                resolve(item);
            });
        });
    }

    load_commands()
    {
        let commands     = new Map();
        let alias_names  = new Array();
        const commandDir = './commands/';

        return new Promise((resolve, _reject) => {
            // Iterate Command Directories and Files
            readdirSync(commandDir).forEach(folder => {
                readdirSync(`${commandDir}${folder}/`).forEach(file => {
                    if (file.match(/\.js$/) !== null && file !== 'index.js')
                    {
                        // Import commands
                        import(`.${commandDir}${folder}/${file}`)
                            .then(module => new (module.default)(this.main))
                            .then(command => {
                                // Check for conflicts
                                if (commands.has(file.toLowerCase()))
                                    console.log(`Command ${command.name} in ${file} is conflicted! Thus it will not be included in the bot.`);
                                else
                                    commands.set(file.slice(0, -3).toLowerCase(), command);

                                // Check for conflicts in command's aliases
                                if (command.alias.length)
                                {
                            command.alias.forEach(item => {
                                if (commands.has(item.toLowerCase()))
                                    console.log(`Command alias ${
                                        item} (Alias of ${command.name} in ${
                                        file}) is conflicted! Thus it will not be included in the bot.`);
                                else
                                {
                                    commands.set(item.toLowerCase(), command);
                                    alias_names.push(item.toLowerCase());
                                }
                            });
                                }
                            }).catch(console.log);
                    }
                });
            });
            resolve([ commands, alias_names ]);
        });
    }
}