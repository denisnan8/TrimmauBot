import fetch from 'node-fetch';

import Command from '../../main/command.js';
import {RAYSHIFT_API, RAYSHIFT_URL} from '../const.js';

const DECK_TYPES         = [ 'normal', 'event' ];
const WATCH_REACTIONS_MS = 60000;

function check_status(fetch_response)
{
    if (fetch_response.ok) return fetch_response;
    throw fetch_response.status;
}

function check_deck_type(args)
{
    return args.find(arg => DECK_TYPES.includes(arg)) || DECK_TYPES[0];
}

function check_discord_user(args, message)
{
    const discord_user = args.join(' ').match(/(?:<@!?)?(\d+)/);
    return (discord_user && discord_user[1]) || message.author.id;
}

// Filter out code only from a string. People tend to add flavor text or
// delimiters to their input.
function check_friend_code(input)
{
    const numbers = input.match(/\d{1}/g);
    return numbers && numbers.slice(0, 9).join('');
}

function swap_deck_type(og_deck_type)
{
    return DECK_TYPES.filter((type) => type !== og_deck_type)[0];
}

export default class FGOProfileRayshift extends Command
{
    // lookup_cached_rayshift_profile(friend_code, placeholder_msg)
    // {
    //     return new Promise((resolve, reject) => {
    //         fetch(`${RAYSHIFT_API}support/decks/${this.region}/${friend_code}`)
    //             .then(res => check_status(res).json())
    //             .then(api => resolve(api.response))
    //             .catch(err => {
    //                 if (typeof err === 'number')
    //                 {
    //                     placeholder_msg.edit(
    //                         'Failed to contact Rayshift.io servers.',
    //                         { embed : null });
    //                     reject(new Error(
    //                         `Failed to contact Rayshift.io servers. HTTP code ${
    //                             err}`));
    //                 }
    //                 else
    //                 {
    //                     reject(err);
    //                 }
    //             });
    //     });
    // }

    refresh_and_lookup_rayshift_profile(friend_code, placeholder_msg, prefix)
    {
        return new Promise((resolve, reject) => {
            const interval = setInterval(() => {
                fetch(`${RAYSHIFT_API}support/lookup?apiKey=${
                          this.main.config.rayshiftToken}&region=${
                          this.region === 'jp' ? 1
                                               : 2}&friendId=${friend_code}`)
                    .then(res => check_status(res).json())
                    .then(api => api.message === 'finished' ? api.response
                                                            : null)
                    .then(api_res => {
                        if (api_res)
                        {
                            clearInterval(interval);
                            resolve(api_res);
                        }
                    })
                    .catch(err => {
                        clearInterval(interval);

                        if (typeof err === 'number')
                        {
                            switch (err)
                            {
                                case 404:
                                {
                                    const fc
                                        = this.main.util.format_friend_code(
                                            friend_code);
                                    placeholder_msg.edit(
                                        `Friend code ${fc} is invalid for the ${
                                            this.region
                                                .toUpperCase()} region. Did you mix up the regions during setup? Set up your profile with \`${
                                            prefix}${
                                            this.region !== 'na'
                                                ? 'us-'
                                                : 'jp-'}profile-edit\` and then try again with \`${
                                            prefix}${
                                            this.region !== 'na'
                                                ? 'us-'
                                                : 'jp-'}rayshift\`.`,
                                        { embed : null });
                                    reject(new Error(
                                        `${fc} is invalid in ${this.region}.`));
                                    break;
                                }
                                // case 503:
                                //     resolve(this.lookup_cached_rayshift_profile(
                                //         friend_code, placeholder_msg, prefix));
                                //     break;
                                default:
                                {
                                    reject(new Error(
                                        `Failed to contact Rayshift.io servers. Maybe it's maintenance? HTTP code ${
                                            err}`));
                                    break;
                                }
                            }
                        }
                        else
                        {
                            reject(err);
                        }
                    });
            }, 1500);    // check back periodically. The API has to load a
                         // few seconds before having the updated lists
        });
    }

    async send_placeholder_message(channel, discord_profile, deck_type)
    {
        return channel.send('Refreshing profile...',
            await this.main.util.fgoProfileRayshift(
                discord_profile,
                { region : this.region, deck_type : deck_type },
                true)
        );
    }

    run(message, args, prefix)
    {
        let deck_type = check_deck_type(args);
        const user    = check_discord_user(args, message);

        Promise
            .all([
                this.main.db.get(`${this.dbKeyPrefix}${user}`),
                this.main.client.users.fetch(user)
            ])
            .then(([ db_profile, discord_profile ]) => {
                if (db_profile)
                {
                    return Promise.all([
                        JSON.parse(db_profile),
                        discord_profile,
                        this.send_placeholder_message(
                            message.channel, discord_profile, deck_type)
                    ]);
                }
                else
                {
                    if (user !== message.author.id)
                    {
                        message.channel.send(
                            'Cannot find profile of requested player. Please recheck your arguments and try again.');
                    }
                    else
                    {
                        message.channel.send(`Profile not found. Please use \`${
                            prefix}${
                            this.region === 'na'
                                ? 'us-'
                                : 'jp-'}profile-edit id:123,456,789\` with **your** friend code to set up a profile.`);
                    }
                    return Promise.reject(
                        `${user} is not in ${this.region} DB.`);
                }
            })
            .then(([ db_profile, discord_profile, placeholder_msg ]) => {
                if (!db_profile.privacy || user === message.author.id)
                {
                    if (db_profile.id)
                    {
                        let friend_code = check_friend_code(db_profile.id);
                        if (friend_code)
                        {
                            return Promise.all([
                                this.refresh_and_lookup_rayshift_profile(
                                    friend_code, placeholder_msg, prefix),
                                discord_profile,
                                placeholder_msg
                            ]);
                        }
                        else
                        {
                            placeholder_msg.edit(
                                `Invalid profile. Please use \`${prefix}${
                                    this.region === 'na'
                                        ? 'us-'
                                        : 'jp-'}profile-edit id:123,456,789\` with **your** friend code to set up a profile.`,
                                { embed : null });
                            return Promise.reject(`${
                                user} has invalid friend code '${
                                db_profile.id}' in profile ${this.region}.`);
                        }
                    }
                    else
                    {
                        placeholder_msg.edit(
                            `Incomplete profile. Please use \`${prefix}${
                                this.region === 'na'
                                    ? 'us-'
                                    : 'jp-'}profile-edit id:123,456,789\` with **your** friend code to set up a profile.`,
                            { embed : null });
                        return Promise.reject(
                            `${user} has no friend code in profile ${
                                this.region}.`);
                    }
                }
                else
                {
                    placeholder_msg.edit(
                        'This player has set their privacy setting to `true`, thus the profile cannot be displayed.',
                        { embed : null });
                    return Promise.reject(`${user} has profile ${
                        this.region} privacy turned on.`);
                }
            })
            .then(async ([ profile_data, discord_profile, placeholder_msg ]) => {
                // editing embeds doesn't seem to be implemented, which is weird
                // cuz it did work in the past and I've seen other bots do it...
                // https://github.com/discordjs/discord.js/issues/5415
                const decks_to_stack = deck_type === 'normal' ? 1 : 8;
                const flags = 8;
                // console.log(profile_data);
                // console.log(`${RAYSHIFT_URL}static/images/deck-gen/${
                    // this.region}/${profile_data.code}/${profile_data.guid}/${decks_to_stack}/${flags}.png`);
                const msg_data = await this.main.util.fgoProfileRayshift(discord_profile, {
                    name : profile_data.name,
                    code : profile_data.code,
                    region : this.region,
                    deck_type : deck_type,
                    lastUpdate : profile_data.lastUpdate,
                    support : `${RAYSHIFT_URL}static/images/deck-gen/${
                        this.region}/${profile_data.code}/${profile_data.guid}/${decks_to_stack}/${flags}.png`
                });
                await placeholder_msg.delete();
                return message.channel.send('', msg_data);
            })
            // .then(async ([ profile_data, discord_profile, placeholder_msg ]) => {
            //     placeholder_msg.react('🔀');

            //     const collector = placeholder_msg.createReactionCollector(
            //         (reaction, user) => reaction.emoji.name === '🔀'
            //                             && user !== message.client.user,
            //         {
            //             time : WATCH_REACTIONS_MS,
            //         });
            //     collector.on('collect', async (_) => {
            //         deck_type = swap_deck_type(deck_type);
            //         placeholder_msg.edit('',
            //             await this.main.util.fgoProfileRayshift(discord_profile, {
            //                 name : profile_data.name,
            //                 code : profile_data.code,
            //                 region : this.region,
            //                 deck_type : deck_type,
            //                 lastUpdate : profile_data.lastUpdate,
            //                 support : `${RAYSHIFT_URL}static/images/deck/${
            //                     this.region}/${profile_data.code}/${
            //                     profile_data.lastUpdate}/${deck_type}_large.png`
            //             })
            //         );
            //     });
            //     collector.on('error', (e) => console.log(e));
            // })
            .catch(console.log);
    }
}