import discordjs_pkg from 'discord.js-light';
const { Client } = discordjs_pkg;

import Config from './config.js';
import {emoji} from './const.js';
import AtlasAcademyApi from './atlas_academy_api.js';
import Database from './db.js';
import Util from './util.js';
import WebServer from './webserver.js';
import heroku_keep_alive from './heroku_keepalive.js';

export default class Bot
{
    constructor(option)
    {
        this.client            = new Client();
        this.config            = new Config(option);
        this.db                = new Database(this.config.dbURL);
        this.atlas_academy_api = new AtlasAcademyApi(this).load_basic_data();
        this.util              = new Util(this);
        this.util.load_commands()
            .then(([ commands, alias_names ]) => {
                this.commands    = commands;
                this.alias_names = alias_names;

                let loginTime = Date.now();
                this.client.on('ready', () => {
                    this.webserver = new WebServer();
                    heroku_keep_alive(this.config.herokuUrl);
                    console.log(
                        `Logged in! Time taken: ${Date.now() - loginTime}ms`);
                });
                this.client.on('disconnect', () => { loginTime = Date.now(); });

                this.client.on('guildMemberAdd', m => {
                    if (!this.config.selfbot)
                    {
                        this.db.get(`config_${m.guild.id}`).then(config => {
                            if (config)
                            {
                                config = JSON.parse(config).welcome;
                                if (config)
                                {
                                    config = config.replace(/\[member]/g, m)
                                                 .replace(/\[guild]/g, m.guild)
                                                 .split(':');
                                    m.guild.channels.get(config[0]).send(
                                        config.slice(1).join(':'));
                                }
                            }
                        });
                    }
                });

                this.client.on('message', message => {
                    if (message.author.bot || !message.guild) return;
                    if (this.config.selfbot
                        && message.author.id != this.client.user.id
                        && message.author.id != this.config.ownerID)
                        return;
                    this.db.get(`config_${message.guild.id}`).then(config => {
                        let prefix = this.config.prefix;
                        if (config)
                        {
                            config = JSON.parse(config);
                            if (config.prefix)
                                prefix = config.prefix;
                            else if (config.prefix === false)
                                prefix = false;
                        }
                        let textPrefix = message.guild.me.displayName;
                        if (prefix)
                            textPrefix = new RegExp(`^${
                                prefix.replace(/[-[\]{}()*+?.,\\^$|#\s]/gi,
                                               '\\$&')}|<@!?${
                                this.client.user.id}>|@${textPrefix}`);
                        else
                        {
                            textPrefix = new RegExp(
                                `<@!?${this.client.user.id}>|@${textPrefix}`);
                            prefix = `@${this.client.user.username}`;
                        }
                        if (!message.content.match(textPrefix)) return;

                        let content
                            = message.content.replace(textPrefix, '').trim();
                        let cleanContent
                            = message.cleanContent.replace(textPrefix, '')
                                  .trim();
                        let args = content.split(' ');

                        let customCommand;
                        if (config && config.commands)
                            customCommand
                                = new Map([...emoji, ...config.commands ]);
                        else
                            customCommand = emoji;
                        let command = this.commands.get(args[0].toLowerCase());
                        if (command)
                        {
                            if (command.cleanContent)
                            {
                                args = cleanContent;
                                if (!command.caseSensitive)
                                    args = args.toLowerCase();
                                args = args.split(' ');
                            }
                            else if (!command.caseSensitive)
                            {
                                args = content.toLowerCase().split(' ');
                            }
                            command.advertise(message, prefix);
                            command.run(message, args.slice(1), prefix);
                            command.timeUsed++;
                            console.log(
                                `${command.name} command has been triggered`);
                        }
                        else if (customCommand.has(args[0]))
                            message.channel.send(customCommand.get(args[0]));
                    });
                });
            })
            .catch(console.log);

        this.client.login(this.config.token).catch(console.log);
    }
}