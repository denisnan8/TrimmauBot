import canvas_pkg from 'canvas';
const { createCanvas, Image } = canvas_pkg;
import discordjs_pkg from 'discord.js-light';
const { MessageAttachment } = discordjs_pkg;
import fetch from 'node-fetch';

import Command from '../../main/command.js';

export default class LoliconCommand extends Command
{
    constructor(main)
    {
        super(main, {
            name : 'lolicon',
            category : 'Image Generation',
            help : 'Onii-chan, are you a lolicon?'
        });
    }
    run(message, args, _prefix)
    {
        fetch('https://i.imgur.com/ulQiHQO.png')
            .then(res => res.buffer())
            .then(r => {
                const canvas  = createCanvas(617, 319);
                const ctx     = canvas.getContext('2d');
                const img_bg  = new Image();
                img_bg.onload = function() {
                    ctx.drawImage(img_bg, 0, 0, 617, 319);
                    ctx.font = 'bold 30px Arial';
                    args     = message.author.username;
                    ctx.fillText(
                        args, 300 - ctx.measureText(args).width / 2, 150);
                    message.channel.send('',
                                         new MessageAttachment(
                                             canvas.toBuffer(),
                                             ));
                };
                img_bg.src = r;
            });
    }
}