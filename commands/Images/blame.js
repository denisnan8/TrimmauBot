import canvas_pkg from 'canvas';
const { createCanvas, Image } = canvas_pkg;

import fetch from 'node-fetch';
import discordjs_pkg from 'discord.js-light';
const { MessageAttachment } = discordjs_pkg;

import Command from '../../main/command.js';

export default class BlameCommand extends Command
{
    constructor(main)
    {
        super(main, {
            name : 'blame',
            category : 'Image Generation',
            help : 'It\'s their fault, not mine. I\'m sure!',
            args : [ {
                name : 'Target',
                desc : 'The name of the victim. Can use mentions.'
            } ],
            caseSensitive : true
        });
    }
    run(message, args, _prefix)
    {
        fetch('https://i.imgur.com/ZjZlp0T.png')
            .then(res => res.buffer())
            .then(r => {
                const canvas = createCanvas(500, 280);
                const ctx    = canvas.getContext('2d');
                const img_bg = new Image();
                ctx.font     = 'bold 18px Arial';
                if (message.mentions.members.first())
                    args = message.mentions.members.first().displayName;
                else
                    args = args.join(' ');
                args = `I-it's your fault ${args}`.trim();
                args += '!';

                img_bg.onload = function() {
                    ctx.drawImage(img_bg, 0, 0, 500, 280);
                    const y       = 260;
                    const x       = 250 - ctx.measureText(args).width / 2;
                    ctx.lineWidth = 3;
                    ctx.strokeText(args, x, y);
                    ctx.fillStyle = 'white';
                    ctx.fillText(args, x, y);

                    message.channel.send(
                        '', new MessageAttachment(canvas.toBuffer()));
                };
                img_bg.src = r;
            });
    }
}