import Command from '../../main/command.js';

export default class FGOProfileEditCommand extends Command
{
    constructor(main) { super(main, { name : 'profile-edit' }); }
    run(message, _, prefix)
    {
        message.channel.send(`This command is deprecated. Use \`${
            prefix}jp-profile-edit\` instead.`);
    }
}