import FGOProfileRayshift from '../../main/cmds/profile-rayshift.js';
import {RAYSHIFT_URL} from '../../main/const.js';

export default class FGOJPProfileRayshiftCommand extends FGOProfileRayshift
{
    constructor(main)
    {
        super(main, {
            name : 'jp-profile-rayshift',
            category : 'Fate Profile',
            alias : [
                'jp-rayshift',
                'jp-rs',
            ],
            help : `Get your F/GO profile through [rayshift.io](${
                RAYSHIFT_URL}) (JP version).`,
            args : [
                {
                    name : 'Player',
                    desc :
                        'Optional. Specify which user\'s profile to request. Their privacy setting has to be turned off.',
                    format : '[@user|user_id]'
                },
                {
                    name : 'Setup',
                    desc :
                        'Optional. Defaults to `normal`. Specify the support deck to be shown.',
                    format : '[normal|event]'
                }
            ]
        });

        this.region      = 'jp';
        this.dbKeyPrefix = 'fgoProfile_';
    }
}
