import Command from '../../main/command.js';

export default class FGOProfileCommand extends Command
{
    constructor(main) { super(main, { name : 'profile' }); }
    run(message, _, prefix)
    {
        message.channel.send(
            `This command is deprecated. Use \`${prefix}jp-profile\` instead.`);
    }
}