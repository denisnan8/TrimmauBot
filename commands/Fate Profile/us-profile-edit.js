import FGOProfileEdit from '../../main/cmds/profile-edit.js';

export default class FGOUSProfileEditCommand extends FGOProfileEdit
{
    constructor(main)
    {
        super(main, {
            name : 'us-profile-edit',
            category : 'Fate Profile',
            alias : [ 'na-profile-edit' ],
            help : 'Save or edit your F/GO profile (NA version).',
            args : [
                { name : 'Name', desc : 'Your IGN.', format : 'name:<IGN>' },
                {
                    name : 'ID',
                    desc : 'Your friend code.',
                    format : 'id:<123,456,789>'
                },
                {
                    name : 'Privacy',
                    desc :
                        'Optional. Defaults to `false`. If set to `false`, everyone can request to see your profile.',
                    format : 'privacy:[true|false]'
                },
                {
                    name : 'Support Image',
                    desc :
                        'The image showing your support list. Attach it to the message with your command.',
                    format : '<Image>'
                }
            ],
            caseSensitive : true
        });

        this.region      = 'na';
        this.dbKeyPrefix = 'fgoUSProfile_';
    }
}