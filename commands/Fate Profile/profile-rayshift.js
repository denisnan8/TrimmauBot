import Command from '../../main/command.js';

export default class FGOProfileRayshiftCommand extends Command
{
    constructor(main)
    {
        super(main, {
            name : 'profile-rayshift',
            alias : [ 'rayshift', 'rs' ],
        });
    }
    run(message, _, prefix)
    {
        message.channel.send(`This command is deprecated. Use \`${
            prefix}jp-profile-rayshift\` instead.`);
    }
}