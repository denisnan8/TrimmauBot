import FGOProfileRayshift from '../../main/cmds/profile-rayshift.js';
import {RAYSHIFT_URL} from '../../main/const.js';

export default class FGOUSProfileRayshiftCommand extends FGOProfileRayshift
{
    constructor(main)
    {
        super(main, {
            name : 'us-profile-rayshift',
            category : 'Fate Profile',
            alias : [
                'us-rayshift',
                'us-rs',
                'na-profile-rayshift',
                'na-rayshift',
                'na-rs'
            ],
            help : `Get your F/GO profile through [rayshift.io](${
                RAYSHIFT_URL}) (NA version).`,
            args : [
                {
                    name : 'Player',
                    desc :
                        'Optional. Specify which user\'s profile to request. Their privacy setting has to be turned off.',
                    format : '[@user|user_id]'
                },
                {
                    name : 'Setup',
                    desc :
                        'Optional. Defaults to `normal`. Specify the support deck to be shown.',
                    format : '[normal|event]'
                }
            ]
        });

        this.region      = 'na';
        this.dbKeyPrefix = 'fgoUSProfile_';
    }
}
