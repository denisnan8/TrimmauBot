import canvas_pkg from 'canvas';
import timed_cache from 'memory-cache';
const { createCanvas, Image } = canvas_pkg;

import fetch from 'node-fetch';
import discordjs_pkg from 'discord.js-light';
const { MessageAttachment } = discordjs_pkg;

import Command from '../../main/command.js';
import {db} from '../../main/const.js';

// in percentages (%)
const RATES = {
    servants_5 : 1,
    servants_4 : 3,
    servants_3 : 40,
    ce_5 : 4,
    ce_4 : 12,
    ce_3 : 40
};
const RATES_ONLY_GOLD = {
    servants_5 : 1,
    servants_4 : 3,
    ce_5 : 4,
    ce_4 : 12,
};
// 5min
const COOLDOWN_MS = 5 * 60 * 1000;

export default class GachaCommand extends Command
{
    constructor(main)
    {
        super(main, {
            name : 'gacha',
            category : 'Fate Grand Order',
            help : 'Gacha hell, but it\'s outside of the game.',
            args : [ {
                name : 'Yolo',
                desc : 'Optional. Include this for a solo roll.',
                format : 'yolo'
            } ]
        });
        this.cooldown      = new timed_cache.Cache();
        this.weighted_card = main.util.weighted_table_generator(RATES);
        this.weighted_card_ensure_gold
            = main.util.weighted_table_generator(RATES_ONLY_GOLD);
    }

    pullCard(cards, ensure_gold)
    {
        let pull = ensure_gold ? this.weighted_card_ensure_gold()
                               : this.weighted_card();
        let type   = pull.split('_')[0];
        let rarity = pull.split('_')[1];

        return `${type === 'ce' ? 'CE/' : 'S/'}${
            this.main.util.ARand(cards[type][rarity])}`;
    }

    roll1(canvas_ctx, cards, pos, ensure_gold = false)
    {
        let card = new Image();
        let item = this.pullCard(cards, ensure_gold);

        return fetch(`${db}images/${item}.png`)
            .then(res => res.buffer())
            .then(r => {
                card.onerror = Promise.reject;
                card.onload = () => {
                    canvas_ctx.drawImage(card, ...pos);
                    if (item.length == 5) item += ' ';
                };
                card.src = r;

                return Promise.resolve(item);
            });
    }

    roll10(canvas_ctx, cards)
    {
        const guaranteed_card_idx = this.main.util.rand(0, 9);
        let results               = Array(10).fill('');
        results                   = results.map((_, index) => {
            const canvas_offset = index < 5 ? [ index * 129, 0 ]
                                            : [ (index - 5) * 129, 222 ];
            return this.roll1(
                canvas_ctx, cards, canvas_offset, index == guaranteed_card_idx);
        });
        return Promise.all(results);
    }

    run(message, args, _prefix)
    {
        fetch(`${db}gatcha.json`).then(res => res.json()).then(r => {
            if (args[0] == 'yolo')
            {
                const canvas = createCanvas(129, 222);
                const ctx    = canvas.getContext('2d');
                this.roll1(ctx, r, [ 0, 0 ]).then((result) => {
                    const attachment = new MessageAttachment(canvas.toBuffer(),
                                                             'result.png');
                    message.channel.send(
                        `The results are in. You get (in card ID):\`\`\`\n${
                            result}\`\`\``,
                        attachment);
                });
            }
            else
            {
                let time = this.cooldown.get(message.author.id)
                           - message.createdTimestamp + COOLDOWN_MS;
                if (time > 0 && message.author.id != this.main.config.ownerID)
                {
                    message.channel.send(
                        `You can only use this command once every 5 minutes. You can use it again in ${
                            Math.floor(time / 60000)} minutes ${
                            Math.ceil(time / 1000) % 60} seconds.`);
                }
                else
                {
                    this.cooldown.put(message.author.id,
                                      message.createdTimestamp,
                                      COOLDOWN_MS);
                    const canvas = createCanvas(645, 444);
                    const ctx    = canvas.getContext('2d');
                    this.roll10(ctx, r).then((results) => {
                        results = results.slice(0, 5).join(' | ') + '\n'
                                  + results.slice(5).join(' | ');

                        const attachment = new MessageAttachment(
                            canvas.toBuffer(), 'result.png');
                        message.channel.send(
                            `The results are in. Here are your cards (in card ID):\`\`\`\n${
                                results}\`\`\``,
                            attachment);
                    });
                }
            }
        });
    }
}