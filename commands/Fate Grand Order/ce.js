import Command from '../../main/command.js';

export default class FGOCECommand extends Command
{
    constructor(main)
    {
        super(main, {
            name : 'ce',
            category : 'Fate Grand Order',
            help : 'Get to see a random CE art.',
            alias : [ 'fgo-ce' ],
        });
    }

    fgoGacha()
    {
        const rarity = this.main.util.rand(1, 5);
        return this.main.atlas_academy_api
            .fetch_item('na', 'equip', rarity, 'nice')
            .then(ce => {
                const img_data = ce.extraAssets.charaGraph.equip;
                return {
                    // Exp cards have this as prefix. It's ugly and useless.
                    name : ce.name.replace('CE EXP Card: ', ''),
                    rarity : rarity,
                    img_url : img_data[Object.keys(img_data)[0]]
                };
            });
    }

    run(message, _args, _prefix)
    {
        this.fgoGacha().then(ce => {
            message.channel.send('', {
                embed : {
                    title : 'Nice!',
                    color : 0xff0000,
                    description : `\u200b\nThe CE ${ce.name} with rarity ${
                        ce.rarity} looks pretty nice!`,
                    image : { url : ce.img_url }
                }
            });
        });
    }
}