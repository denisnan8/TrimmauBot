import Command from '../../main/command.js';

export default class HelpCommand extends Command
{
    constructor(main)
    {
        super(main, {
            name : 'help',
            category : 'Bot Info',
            help :
                'Show list of all commands, or more info about a certain command.',
            args : [ {
                name : 'Command',
                desc :
                    'Optional. Will show the list of all commands if omitted.'
            } ]
        });
    }
    run(message, args, prefix)
    {
        args = args.join(' ');
        if ((args = this.main.commands.get(args)) && !args.devOnly)
        {
            const CMD_FMT_IDX
                = 1;    // args.help.fields is hardcoded in `Command` class
            const command_format = args.help.fields[CMD_FMT_IDX].value;
            args.help.fields[CMD_FMT_IDX].value = `${
                command_format.slice(0, 1)}${prefix}${command_format.slice(1)}`;

            message.channel.send('', { embed : args.help });
        }
        else
        {
            let embed = {
                title : 'Command lists',
                description : `Use \`${
                    prefix}help <command>\` to see more about that specific command.`,
                fields : []
            };
            let categories = {};
            this.main.commands.forEach((item, name) => {
                if (!this.main.alias_names.includes(name) && !item.devOnly)
                {
                    if (!categories[item.category])
                        categories[item.category] = [];
                    categories[item.category].push(name);
                }
            });
            for (let item in categories)
            {
                let ctg   = [ [] ];
                let index = 0;
                categories[item].forEach(command => {
                    args = `${ctg[index].join(', ')}, ${command}`;
                    if (args.length > 1024)
                    {
                        index++;
                        ctg[index] = [];
                    }
                    ctg[index].push(`${command}`);
                });
                ctg.forEach((command, index) => {
                    if (ctg.length > 1)
                        index = `${item} (${index + 1})`;
                    else
                        index = item;
                    embed.fields.push(
                        { name : index, value : command.join(', ') });
                });
            }
            message.channel.send('', { embed });
        }
    }
}