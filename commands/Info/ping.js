import Command from '../../main/command.js';

export default class PingCommand extends Command
{
    constructor(main)
    {
        super(main, {
            name : 'ping',
            category : 'Bot Info',
            help : 'Poke the bot to see if it\'s alive.'
        });
    }
    run(message, _args, _prefix)
    {
        message.channel.send('Pinging...').then(msg => {
            msg.edit(`Pong! It took ${
                msg.createdTimestamp - message.createdTimestamp}ms.`);
        });
    }
}