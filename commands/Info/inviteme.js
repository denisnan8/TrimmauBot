import Command from '../../main/command.js';

export default class InviteMeCommand extends Command
{
    constructor(main)
    {
        super(main, {
            name : 'inviteme',
            category : 'Bot Info',
            help : 'Display the link that you can use to invite the bot.'
        });
    }
    run(message, _args, _prefix)
    {
        message.channel.send(
            'To invite the bot, use this link: https://discord.com/oauth2/authorize?client_id=672550179529818112&scope=bot&permissions=125984');
    }
}