import Command from '../../main/command.js';
export default class SayCommand extends Command
{
    constructor(main)
    {
        super(main, { name : 'say', devOnly : true, caseSensitive : true });
    }
    // Sends message as the bot. No docs due to them not showing up in help
    // anyway.
    // $say <snowflake> <message>
    run(message, args, _prefix)
    {
        if (message.author.id == this.main.config.ownerID)
        {
            if (args && args.length > 1)
            {
                this.main.client.channels.fetch(args[0]).then(
                    (channel) => { channel.send(args.slice(1).join(' ')); });
            }
        }
    }
}