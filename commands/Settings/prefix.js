import Command from '../../main/command.js';

export default class CustomCommand extends Command
{
    constructor(main)
    {
        super(main, {
            name : 'prefix',
            category : 'Setting',
            args : [ {
                name : 'Prefix',
                desc :
                    'The new prefix to use. Use `[disable]` to change the prefix to mentioning the bot.'
            } ],
            help : 'Set your guild\'s custom prefix.',
            caseSensitive : true
        });
    }
    run(message, args, _prefix)
    {
        if (!message.member.hasPermission('MANAGE_GUILD'))
        {
            return message.channel.send(
                'Error: You need MANAGE SERVER permission to use this command.');
        }
        args = args.join(' ');
        this.main.db.get(`config_${message.guild.id}`).then(config => {
            if (config)
                config = JSON.parse(config);
            else
                config = {};

            args = args || `@${this.main.client.user.username}`;
            if (args == '[disable]')
                args = `@${this.main.client.user.username}`;

            config.prefix = args;
            args = `Prefix has been changed successfully! The new prefix is \`${
                args}\`.`;

            this.main.db
                .set(`config_${message.guild.id}`, JSON.stringify(config))
                .then(() => { message.channel.send(args); });
        });
    }
}