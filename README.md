# TrimmauBot
Customized fork of https://github.com/aister/nobuBot

# Setup
Node-modules are not included.

To set-up the bot:
1. install and configure a Redis database
2. clone the repo and run `npm install` in its root directory
3. set your config options (see ``main/config.js``) in ``index.json`` or by means of environment variables
4. run ``npm run start`` in the root directory

When hosting on a non-heroku environment, `webserver` and `heroku_keepalive` may be removed.

Please keep your login tokens absolutely secret!